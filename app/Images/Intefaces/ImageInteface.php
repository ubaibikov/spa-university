<?php

namespace App\Images\Intefaces;

interface ImageInteface
{
    public function createImage();
    public function updateImage(string $lastimageWithPath);
    public function deleteImage(string $imagePath);
}
