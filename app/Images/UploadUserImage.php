<?php

namespace App\Images;

use App\Images\UploadUserAvatarImage;
use App\Images\ImageCreator;

class UploadUserImage
{
    private $uploadUserAvatarImage;
    private $imageCreator;

    public function __construct(string $userId, $requestFile)
    {
        $this->uploadUserAvatarImage = new UploadUserAvatarImage($userId, $requestFile->getClientOriginalExtension());

        $userAvatarImageName = $this->uploadUserAvatarImage->getUserAvatarImageName();

        $userAvatarImagePath = $this->uploadUserAvatarImage->getUserAvatarImagePath();
        $this->imageCreator = new ImageCreator($requestFile, $userAvatarImagePath, $userAvatarImageName);
    }

    /**
     * @method upload Uploading user avatar image
     * @param string|null $lastimageWithPath if user update your image
     */
    public function upload(string $lastimageWithPath = null)
    {
        return $this->uploadUserAvatarImage->getUserAvatarImagePath();
    }
}
