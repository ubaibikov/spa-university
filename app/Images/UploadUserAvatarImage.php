<?php

namespace App\Images;

class UploadUserAvatarImage
{
    protected $userId;
    protected $imageExtension;
    protected $imageName;
    protected $imagePath;

    public function __construct(string $userId, string $imageExtension)
    {
        $this->userId = $userId;
        $this->imageExtension = $imageExtension;
    }

    public function getUserAvatarImageName(): string
    {
        $this->imageName = time() . '__currentUserAvatarImage__' . $this->userId . '.' . $this->imageExtension;
        return $this->imageName;
    }

    public function getUserAvatarImagePath(): string
    {
        $this->imagePath = storage_path('images/userImages/' . $this->userId);
        return $this->imagePath;
    }

    protected function getPathExample():string
    {
        return 'images\userImages\\' . $this->userId;
    }
}
