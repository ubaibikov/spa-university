<?php

namespace App\Images;

use App\Images\Intefaces\ImageInteface;
use Illuminate\Support\Facades\File;

class ImageCreator implements ImageInteface
{
    protected $requestImage;
    protected $uploadPath;
    protected $imageName;

    public function __construct($requestImage, string $uploadPath, string $imageName)
    {
        $this->requestImage = $requestImage;
        $this->uploadPath = $uploadPath;
        $this->imageName = $imageName;
    }

    public function createImage()
    {
        return $this->requestImage->move($this->uploadPath, $this->imageName);
    }

    public function updateImage(string $lastimageWithPath)
    {
        $this->deleteImage($lastimageWithPath);
        return $this->createImage($this->uploadPath, $this->newImageName);
    }

    public function deleteImage(string $imagePath)
    {
        return File::delete($imagePath);
    }

    public function getImageRelativePath(): string
    {
        return $this->uploadPath . $this->imageName;
    }
}
