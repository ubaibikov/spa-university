<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\ProfileSettings;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

use App\Images\UploadUserImage;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function update(Request $request)
    {
        if ($request->file) {

            $image = $request->file('file');

            $filename = date('Y-m-d-H:i:s').'.'.$image->getClientOriginalExtension();
            $img = Image::make($image->path());
            $photo = $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            // Storage::disk('public')->put('',$img);

            return response()->json([
                'uploadImage' => true,
                'message' => 'Upload image return success',
                'status' => 200,
                // 'data' => $uploadImage->upload()
            ]);
        }

        $profileUser = ProfileSettings::where('user_id', auth()->user()->id)->get()->first();
        if (!$profileUser) {
            ProfileSettings::create($request->data['profileSettings']);
        } else {
            ProfileSettings::where('user_id', auth()->user()->id)->update($request->data['profileSettings']);
        }

        return response()->json(['status' => 200, 'profileSettings' => $request->data['profileSettings']]);
    }

    public function getSettings()
    {
        $profileSettings = ProfileSettings::where('user_id', auth()->user()->id)
            ->select('user_id', 'university_id', 'country_id', 'profile_status')
            ->get()
            ->first();

        if (!$profileSettings) {
            return response()->json(['status' => 404]);
        }

        return response()->json($profileSettings);
    }
}
