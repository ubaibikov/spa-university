<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileSettings extends Model
{
    protected $fillable = [
        'user_id',
        'profile_status',
        'relative_image_path',
        'country_id',
        'university_id',
    ];
}
