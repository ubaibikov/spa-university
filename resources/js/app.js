require('./bootstrap');

import App from './components/App';
import VueRouter from 'vue-router';
import router from './router';
import axios from 'axios';
import Api from './api.js';
import Auth from './auth.js';
import vueTopprogress from 'vue-top-progress'

import 'materialize-css'
import 'materialize-css/dist/css/materialize.css'

document.addEventListener('DOMContentLoaded', function() {
    M.AutoInit();
    M.updateTextFields();
 });

window.api = new Api();
window.auth = new Auth();
window.Vue = require('vue');
window.Event = new Vue;

Vue.use('axios');
Vue.use(VueRouter);
Vue.use(vueTopprogress)

const app = new Vue({
    router,
    el: '#app',
    components: {
      App
    },
    render: h => h(App),
  });
