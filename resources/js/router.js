import VueRouter from 'vue-router';
import Home from './components/Home';
import Registration from './components/users/Registration';
import Login from './components/users/Login';
import Start from './components/users/Start';
import ProfileSettings from './components/users/ProfileSettings'
let routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/registration',
        component: Registration,
        meta: { auth: true }
    },
    {
        path: '/login',
        component: Login,
        meta: { auth: true }
    },
    {
        path: '/home',
        component: Start,
        meta: { middlewareAuth: true }
    },
    {
        path: '/profile-settings',
        component: ProfileSettings,
        meta:{middlewareAuth:true}
    }
];


const router = new VueRouter({
    routes,
    history: false

});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.middlewareAuth)) {
        if (!auth.check()) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });

            return;
        }
    }

    if (to.matched.some(record => record.meta.auth)) {
        if (auth.check()) {
            next({
                path: '/home',
                query: { redirect: to.fullPath }
            })
        }
    }
    next();
});

export default router;
