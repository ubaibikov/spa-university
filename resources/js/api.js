class Api {
    constructor () {}

    call (requestType, url, data = null, headers) {
        return new Promise((resolve, reject) => {
            axios[requestType](url, data, headers)
                .then(response => {
                    resolve(response);
                })
        });
    }

    getAuthorizationHeaders(token){
        return {headers: this.getAuthorizationBearer(token)};
    }

    getAuthorizationBearer(token){
        return {'Authorization': `Bearer ${token}`};
    }
}

export default Api;
